import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {StyleSheet} from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button,Text, Left, Body, Title, Right, Tabs, Tab, TabHeading} from 'native-base';
import Home from './components/Home';
import Task from './components/Task';
import Fan from './components/Fan';
export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {index: 0} // default screen index
     }
  
     switchScreen(index) {
        this.setState({index: index})
     }
  

  render() {
    
    let AppComponent = null;

    if (this.state.index == 0) {
       AppComponent = Home
    } else if(this.state.index == 1){
       AppComponent = Task
    }
    else if (this.state.index == 2){
        AppComponent = Fan
    }

    const {iconStyle} = style
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon 
              style = {iconStyle}
              name='chevron-left' />
            </Button>
          </Left>
          <Body>
            <Title>Header</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon 
              style = {iconStyle}
              name='bars' />
              {/* <Icon name='ellipsis-h' /> */}
            </Button>
          </Right>
        </Header>
        <Content>
            <AppComponent />
        </Content>
        <Footer>
          <FooterTab>
            <Button 
            onPress={() => this.switchScreen(0) }
            vertical>
              <Icon style = {iconStyle} name="home" />
              <Text>Home</Text>
              {/* {this.state.visible && <Content><Home/></Content>} */}
            </Button>
            <Button 
            onPress={() => this.switchScreen(1) }
            vertical>
              <Icon style = {iconStyle} name="tasks" />
              <Text>Task</Text>
            </Button>
            <Button 
            onPress={() => this.switchScreen(2) }
            vertical 
            >
              <Icon style = {iconStyle} name="plus" />
              <Text>Fan</Text>
            </Button>
            
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const style = StyleSheet.create({
    iconStyle : {
        fontSize : 20,
        color : '#1E90FF'
    }
})