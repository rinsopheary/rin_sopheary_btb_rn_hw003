// import { Button } from "native-base";
// import React, { Component } from "react";
// import { StyleSheet, Image, View, Animated, TouchableWithoutFeedback , Text, Easing} from "react-native";
// let logo = 'https://images.vexels.com/media/users/3/136532/isolated/preview/93b5c734e3776dd11f18ca2c42c54000-owl-round-icon-by-vexels.png'

// let aa = new Animated.Value(0);
// const rotateInterPolate = aa.interpolate({
//     inputRange: [0, 350],
//     outputRange: ["0deg", "360deg"],
//   })
//   const animatedStyles = {
//     transform: [{ rotate: rotateInterPolate }],
//   };
// export default class animations extends Component {
// //   state = {
// //     animation: new Animated.Value(0),

// //   };
//   startAnimation = () => {
//       for (let index = 0; index < 10; index++) {
//         aa.setValue(0)
//         Animated.loop(
//             Animated.timing(aa, {
//                 toValue: 3000,
//                 duration: 3000,
//                 easing : Easing.linear,
//                 useNativeDriver : false
//               })
//         ).start();

//       }
//   };


//   render() {


//     return (
//       <View style={styles.container}>
//         {/* <TouchableWithoutFeedback onPress={this.startAnimation}> */}
//           <Animated.View style={[styles.box, animatedStyles]} >
//             <Image style={{ width: 150, height: 150 }} source={require('../assets/images/fanflate.jpg')}></Image>
//           </Animated.View>
//         {/* </TouchableWithoutFeedback> */}

//         <Button onPress={this.startAnimation}><Text>Start</Text></Button>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: "center",
//     justifyContent: "center",
//   },
//   box: {
//     width: 150,
//     height: 150,

//   },
// });


//Way2: 
import React from 'react'
import { StyleSheet, Text, View, Animated, Easing, Image } from 'react-native'
import { Button, Row } from 'native-base'

const Fan = () => {
    spinValue = new Animated.Value(0);

    // First set up animation 
    const startAnimation = () => {
        Animated.loop(
            Animated.timing(
                this.spinValue,
                {
                    toValue: 10,
                    duration: 3000,
                    easing: Easing.linear,
                    useNativeDriver: true
                }
            )
        ).start()
    }

    const stopAnimation = () => {
        Animated.timing(
            this.spinValue,
            {
                toValue: 0,
                //  easing: Easing.linear,
                useNativeDriver: true
            }
        ).stop()

    }

    // Next, interpolate beginning and end values (in this case 0 and 1)
    const spin = this.spinValue.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
    })
    const {container, buttonContainer} = styles
    return (
        <View style={container}>
            <Animated.Image
                style={{ transform: [{ rotate: spin }], width: 300, height: 300 }}
                source={require('../assets/images/slab.png')} />

            <Image
                style={{ width: 150, height: 150}}
                source={require('../assets/images/fan2.png')}
            />
            
            <View style ={buttonContainer}>
                <View>
                    <Button
                    onPress={startAnimation}
                    success
                    rounded
                    >
                        <Text style={{textAlign: "center", padding: 10}}>ON</Text>
                    </Button>
                </View>

                <View style = {{paddingLeft: 10}}>
                    <Button
                    onPress={stopAnimation}
                    danger
                    rounded
                    >
                        <Text style={{textAlign: "center", padding: 10}}>OFF</Text>
                    </Button>
                </View>
            </View>


        </View>

    )
}

export default Fan

const styles = StyleSheet.create({
    container : {
        padding : 10,
        paddingTop : 50,
        alignItems : "center",
    },
    buttonContainer : {
        padding : 20,
        flexDirection : "row"
    },
    styleFan1 : {

    },

    styleFan2 : {

    }
})


