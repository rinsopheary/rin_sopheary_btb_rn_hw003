import React, { Component } from 'react'
import { StyleSheet, Image, View, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import { Card, CardItem, Thumbnail, Text, Button, Left, Body } from 'native-base';

export class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      like: 0,
      color: '#1E90FF'
    }
  }

  handlerLike = () => {
    this.setState({
      like: this.state.like + 1,
      color: this.state.color
    })
  }

  render() {
    const { imageStyle, containerImg } = styles
    return (
      <Card style={{ flex: 0 }}>
        <CardItem>
          <Left>
            <Thumbnail source={require("../assets/images/pheary.jpg")} />
            <Body>
              <Text>Rin Sopheary</Text>
              <Text note>២៥ តុលា ២០២០</Text>
            </Body>
          </Left>
        </CardItem>
        <CardItem>
          <Body>
            <View style={containerImg}>
              <Image
                source={require("../assets/images/farm.jpeg")}
                // source = {{uri : 'https://d3h30waly5w5yx.cloudfront.net/images/main/emtheme/background/fruit-picking-korea-og.jpg?v=161122'}}
                style={imageStyle}
                PlaceholderContent={<ActivityIndicator />}
              />
            </View>
            <Text>
              ការបង្កើតកសិកម្មនៅប្រវត្តិសាស្រ្តគឺមានការជាប់ទាក់ទង់និងការផ្សាំងនៃចម្ការ និងសត្វដែលបានអភិវឌ្ឍជាង ១២,០០០ ឆ្នាំមកហើយ តែយ៉ាងណាក៏ដោយមនុស្សនៅតែបានចាប់ផ្តើមមានការផ្លាស់ប្តូរ រុក្ខជាតិ និងសត្វឱ្យមានការទំនាក់ទំនង់គ្នា និងដើម្បីបង្កើតគុណប្រយោជន៍តាមអត្តន័យទៅតាមការបង្កើតដីស្រែឲ្យមានភាពរាប់ស្មើ។
                </Text>
          </Body>
        </CardItem>
        <CardItem>
          <Left>
            <Button
              onPress={this.handlerLike}
              transparent >
              <Icon
                color={this.state.color}
                size={20}
                name="thumbs-up" />
            </Button>
            <Text>{this.state.like > 1 ? `${this.state.like} Likes` : `${this.state.like} Like`}</Text>
          </Left>
        </CardItem>
      </Card>
    )
  }
}

const styles = StyleSheet.create({
  imageStyle: {
    resizeMode: "contain",
    flex: 1,
    width: '100%',
    height: '100%'
  },
  containerImg: {
    width: 380,
    height: 200
  }
})

export default Home
