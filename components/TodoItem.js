import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

const TodoItem = ({item, deleteHandler}) => {

    const {container, heading, content} = styles

    return (
        <View style={container}>
            <View>
                <Text style={heading}>{item.title }{"\n"}</Text>
                <Text style={content}>{item.description}</Text>
            </View>
            <View>
                <TouchableOpacity
                onPress = {() => {deleteHandler(item.key)}}
                >
                <Icon 
                size = {30}
                color = '#bb2205'
                name="trash"/>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default TodoItem

const styles = StyleSheet.create({
    container : {
        padding : 10,
        backgroundColor : '#EEEEEE',
        marginBottom : 7,
        flexDirection : "row",
        justifyContent: "space-between",
        alignItems : "center",
        borderRadius : 2
    },
    heading : {
        fontWeight : "bold",
        fontSize : 16
    },
    content : {

    }
})
