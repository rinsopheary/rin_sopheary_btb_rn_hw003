import React, { useState } from 'react'
import { Form, Item, Input, Label, Button, Text } from 'native-base'
import { StyleSheet, View } from 'react-native'

const AddTodo = ({item, addHandler}) => {
    
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')

    const titleChangeHandler = (val) =>{
        setTitle(val)
    }

    const descChangeHandler = (val) => {
        setDescription(val)
    }

    const { headingStyle } = styles
    return (
        <View>
            <Form>
                <Text style={headingStyle}>Add Task</Text>
                <Item floatingLabel>
                    <Label>Title</Label>
                    <Input 
                    onChangeText={titleChangeHandler}
                    />
                </Item>
                <Item floatingLabel last>
                    <Label>Description</Label>
                    <Input 
                      onChangeText = {descChangeHandler}  
                    />
                </Item>
                <Button
                    style={{ marginTop: 20, marginBottom : 20}}
                    onPress = {()=> addHandler(title, description)}
                    block>
                    <Text>Add</Text>
                </Button>
            </Form>
        </View>
    )
}

export default AddTodo

const styles = StyleSheet.create({
    headingStyle: {
        fontSize: 20,
        fontWeight: "bold"
    }
})
