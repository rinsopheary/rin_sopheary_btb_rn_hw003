import React, { Component } from 'react'
import { Text, View, Button } from 'react-native'

export class Home2 extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             value : 1
        }
    }

    handlerIncrease = () =>{
        this.setState({
            value : 2
        })
    }
    
    render() {
        return (
            <View>
                <Text>{this.state.value}</Text>
                <Button 
                    onPress = {()=>{
                        // Alert.alert(`${this.state.value}`)
                        this.setState ({value : this.state.value + 1})
                    }}
                    title  = 'sth'
                />
            </View>
        )
    }
}

export default Home2
