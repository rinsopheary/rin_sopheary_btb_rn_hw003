import React, { useState } from 'react'
import { Alert, FlatList, StyleSheet, View } from 'react-native';
import TodoItem from './TodoItem';
import AddTodo from './AddTodo';

const Task = () => {
    const [todo, setTodo] = useState([
        // { title: 'Movie', description: 'Legend cinema with my friends', key: '1' },
        // { title: 'Game', description: 'Play at cafe then eat pizza', key: '2' },
    ])

    const deleteHandler = (key) => {
        setTodo((prevTodo) => {
            return prevTodo.filter(todo => todo.key != key)
        })
    }

    const addHandler = (title, description) => {
        setTodo((prevTodo) => {
            return [
                { title: title, description: description, key: Math.random().toString() },
                ...prevTodo
            ]
        })
    }

    const { container } = style
    return (
        <View style={container}>
            <AddTodo addHandler={addHandler} />
            <View>
                <FlatList
                    data={todo}
                    renderItem={({ item }) => (
                        <TodoItem item={item} deleteHandler={deleteHandler} />
                    )
                    }
                />
            </View>

        </View>
    )
}

export default Task

const style = StyleSheet.create({
    container: {
        padding: 20
    },
})

