import {Button, Form, H3, Input, Item, Label} from 'native-base';
import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';


export class Test extends PureComponent {
    constructor(props) {
        super(props)

        this.state = {
            topic: '',
            topicError: '',
            errorMsg: false,
        }
    }


    validate = () => {
        let topicError = '';
        if (!this.state.topic) {
            topicError = '*Topic Type Cannot Be Blank';
        }
        if (topicError) {
            this.setState({
                topicError,
            });
            return false;
        }
        return true;
    };
    _mySubmit = () => {
        if (this.validate()) {
            alert(this.state.topic);
        } else {
            this.setState({
                errorMsg: true,
            });
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <Form>
                    <Item inlineLabel>
                        <Label>Username</Label>
                        <Input
                            onChangeText={(e) => this.setState({ topic: e })}
                            value={this.state.topic}
                        />
                    </Item>
                    {!this.state.topic ? (
                        <Text
                            style={{
                                display: this.state.errorMsg ? 'flex' : 'none',
                                color: 'red',
                            }}>
                            {this.state.topicError}
                        </Text>
                    ) : null}          </Form>
                <Button onPress={this._mySubmit}>
                    <Text>Submit</Text>
                </Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({})

export default Test

